from rapidroute.device_toolkit import *

if __name__ == "__main__":
    init(10)

    new_design("torus_test", "xcku115-flva1517-3-e")
    load_template("src/main/resources/default-templates/dcps-xcku115-flva1517-3-e")

    xs = ["11", "25", "39", "53", "70", "87"]
    ys = ["20", "60", "100", "140", "180", "220"]

    new_design("torus_test", "xcku115-flva1517-3-e")
    load_template("src/main/resources/default-templates/dcps-xcku115-flva1517-3-e")

    for x in xs:
        for y in ys:
            add_register("reg_" + x + "_" + y,
                [create_component("west_8b", "SLICE_X" + x + "Y" + y)])

    place_design()
    for x in xs:
        for i in range(-1, len(ys) - 1):
            add_connection("reg_" + x + "_" + ys[i], "reg_" + x + "_" + ys[i + 1], (0, 3), (0, 3))
    for y in ys:
        for i in range(-1, len(xs) - 1):
            add_connection("reg_" + xs[i] + "_" + y, "reg_" + xs[i + 1] + "_" + y, (4, 7), (4, 7))

    net_synthesis()
    route_design()

    write_checkpoint("torus_test_6x6.dcp");
