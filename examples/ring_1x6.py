from rapidroute.device_toolkit import *

if __name__ == "__main__":
    init(10)

    x = "71"
    ys = ["20", "44", "68", "92", "116", "140", "164", "188", "212", "236"]

    new_design("register_pair_test", "xcku115-flva1517-3-e")
    load_template("src/main/resources/default-templates/dcps-xcku115-flva1517-3-e")

    for i in range(len(ys)):
        add_register("reg" + str(i), [create_component("west_8b", "SLICE_X" + x + "Y" + ys[i])])

    for i in range(0, len(ys) - 1):
        add_connection("reg" + str(i), "reg" + str(i + 1))
    add_connection("reg" + str(len(ys) - 1), "reg0")

    place_design()

    net_synthesis()
    route_design()

    write_checkpoint("rings_test_1x6.dcp");
